const express = require('express');
const cors = require('cors');
const fb = require('./config/db')


// crear el servidor
const app = express();
app.set("port", process.env.PORT || 3000);

// habilitar cors
app.use(cors());
// Habilitar express.json
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use((_, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "X-API-KEY, Origin, X-Requested-Width, Content-Type, Accept,Access-Control-Request-Method,Authorization"
    );
    res.header("Access-Control-Request-Methods", "GET, POST,OPTIONS,PUT,DELETE");
    res.header("Allow", "GET, POST,OPTIONS,PUT,DELETE");
    next();
});

// Conectar a la base de datos

// puerto de la app
const PORT = process.env.PORT || 5000;
// Importar rutas
app.use('/', (rep, res) => {
    res.send("servidor montado")
});

// app.use('/api/usuarios', require('./routes/usuarios'));
// app.use('/api/proyectos', require('./routes/proyectos'));
// app.use('/api/tareas', require('./routes/tareas'));

// arrancar la app
app.listen(app.get('port'));
console.log(`El servidor esta funcionando en el puerto ${app.get('port')}`);
