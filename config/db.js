const firebase = require('firebase/app');
require('firebase/auth');
require('firebase/firestore');
require('firebase/storage');

const firebaseConfig = require('./config');



// const Firebase = () => {
//     try {
//         let app = null;
//         if (!firebase.apps.length) {
//             app = firebase.initializeApp(firebaseConfig)
//         }
//         console.log("conectado a firbase");
//         return {
//             auth: app.auth(),
//             db: app.firestore(),
//             storage: app.storage(),
//         };
//     } catch (error) {
//         console.log(error);
//         return null;
//     }
// }


class Firebase {
    
        constructor() {
                let app = null;
        if (!firebase.apps.length) {
                app = firebase.initializeApp(firebaseConfig)
        }
        this.auth = app.auth();
        this.db = app.firestore();
        this.storage = app.storage();
    }

}

const fb = new Firebase();


module.exports = fb;
// export default fb;
// export { firebase }